#!/bin/sh

mkdir -p pngs

for pdf in pdfs/*pdf
do
	name=$(basename -s .pdf "$pdf")
	convert -density 192 "$pdf" -trim +repage pngs/"$name".png
done
