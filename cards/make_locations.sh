#!/bin/sh

./setup.sh

cat ../locations.csv | grep -v '#' | while read -r card 
do
	name="$(echo $card | cut -d'|' -f1)"
	output="pdfs/$name.tex"
	rule="$(echo $card | cut -d'|' -f2)"
	subtext="$(echo $card | cut -d'|' -f3)"
	
	cp locations.tex "$output"
	sed -i "s#NAME#$name#g" "$output"
	sed -i "s#RULE#$rule#g" "$output"
	sed -i "s#SUBTEXT#$subtext#g" "$output"
	sed -i "s#IMAGE#$name.png#g" "$output"

done
