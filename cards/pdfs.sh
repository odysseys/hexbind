#!/bin/sh

cd pdfs

for t in *tex
do
	pdflatex "$t"
done
