#!/bin/sh

mkdir -p pdfs img

[ ! -e pdfs/lib ] && ln -s ../lib pdfs/lib
[ ! -e pdfs/img ] && ln -s ../img pdfs/img
