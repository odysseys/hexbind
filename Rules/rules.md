# Overview

A game for 3-6 friends, who will soon be enemies.

# Setup {#setup}

Let's make a world.

## The Map

Place the map piece with the tavern in the centre of your table, and take the remaining map pieces.
Take it in turns to place map pieces onto the board until each player has placed two (including the tavern), and you have at least 9 pieces on the board.

- The player who was most recently abroad lays the first tile.
- Each piece must join to another by at least two sides.

You have now built a map.
Around the edge of the map lies sea.
If you can't see the sea, you should paint your table blue.

## Pieces

1. Randomly draw three Locations, \gls{character}, and Items.
    - Players take turns drawing, until they all have enough.
1. Place the cards on the Quest Zone.
1. Give each Card a Marker.
    - The first Location Card receives a Red Pyramid, the next receives a Green Pyramid, and the last receives a Blue Pyramid.
    - The first \gls{character} Card receives a Red Meeple, the next receives a Green Meeple, and the last receives a Blue Meeple.
    - The first Item Card receives a Red Cube, the next receives a Green Cube, and the last receives a Blue Cube.
1. Place corresponding Markers on the board, to represent the Cards' places on the map.
    - Each player takes it in turn to place a piece until all have been placed.
    - Each piece must go in the centre of any tile.
    - Locations cannot go onto the tavern.

## \Glsfmtlongpl{pc}

1. Each player draws a random Card from the \gls{character} Deck.
    - They take their \gls{character}'s corresponding Marker, and place it on a stand.
1. Players take it turns to place their \gls{character}'s marker on the board.
    - \Glspl{character} must go by the sea.
    - Each \gls{pc} must have a full tile between them and any other \gls{pc}.
1. Each player draws a Quest Card.
    - Keep Quest Card Secret.
    - To win the game, complete what is written on the Quest Card, and reveal it to the other players.

## \Glsfmtlongpl{rps}

- Each player receives a set of \glspl{rps} for Combat.

## Barter

1. Find a place mat.
1. Draw and place 3 random Item Cards on it to represent the items available for barter.
    - This is now the 'Barter Table'.

## Reaching Things

Everything you need should sit within easy reach.

- Decks
    * Exploration Deck
    * \Gls{character} Deck
    * Items Deck
- \Gls{monster} tiles

### Tidying the Board

- Put the location cards back in the box.

The Quest can now begin.

## Designations

- Look to your left.
    That player is your \gls{nemesis}.
    They will play cards throughout the game to annoy and frustrate you.
- Look to your right.
    That player is your target.
    When you get a card in your hand, you can target them or yourself.

# Play

## The Turn

1. \Gls{nemesis} performs up to one of these actions:
    - play cards from their hands, or
    - make any \gls{monster} on the board take an action (see <#monsters>).
1. Player select one action:
    - Fight any creature on your tile.
    - Pick up any number of items on your hex.
    - Explore
    - Cast a spell (See <#magic>)
    - Gift items to a \gls{character} or \gls{monster}
    - Persuade \pgls{npc} to join you
    - Request an ally take an action
1. \Gls{pc} moves (see <#movement>)

## Actions

### Exploration

- When you explore an area, draw a single Exploration Card.

### \Glsfmtplural{monster} {#monsters}

- To play a \gls{monster} card, \pgls{nemesis} must discard a number of cards equal to the \gls{monster}'s Rating.
    * \Glspl{nemesis} can play \gls{monster} cards on any tile in the game.
    * Keep the \gls{monster} Card at the side of the board until someone slays it.
- When \glspl{monster} inhabit the same tile as \pgls{pc}, the \gls{monster} attacks.
    * Once the \gls{monster} dies, the card enters the Exploration Graveyard.

### Items

- During your \gls{nemesis} phase, you can play item cards face-down next to your \gls{character} card.
- You can flip it over at any point in order to use it.
- You can carry a number of items equal to your Carry score, which is equal to your Strength + 2.
- \Gls{character} can only carry 1 Large item.
    * If you cannot carry an item, it goes to the item discard pile immediately.

### Barter

1. Players can barter in any town.
1. To pick up a card from the Barter Table, discard card with a total gp value, equal to or greater than the value of the card you want.
    * Once you have purchased Item Cards, place them face-up next to your \gls{pc} Card.
1. Once the bartering has finished, replenish the Bartering Table with three items.
    * If a magical item would enter the Bartering Table, discard it.

### Gift

- You can gift any number of items your \gls{pc} currently carries to anyone else on the same hex.
    * If your items are face-down, keep them face-down when giving them away.

### Persuade

- You can try to persuade one of the three \glspl{npc} to follow you on your quest, as long as you are on the same hex.
    * If your Persuasion score is higher than theirs, they join you.
    * Everyone's Persuasion score starts as the same as their Wits, but might be changed by items.
    * If your Persuasion score drops to equal or lower than your Ally's, they stop being your Ally.
    * If you leave the same hex as an Ally, they stop being your Ally.
        - Therefore, when you travel, you can only move as fast as the slowest \gls{character} in your entourage, if you don't want to lose Allies.

### Request

- If you persuade someone to follow you, then you can have them take an action instead of you, as long as you are both on the same hex.
    * They can do every type of action you can do, except Explore.
    * They have no real-world hands, so they cannot hold Cards.

## Movement {#movement}

- You may move your \gls{pc} across a number of tiles equal to your Movement rating.
    * Your Movement starts as the same as your Dexterity, but might increase later.
- If you move onto (or through) a Mountain, your Movement is halved (rounded up).
- If you are on a town by the sea, you can spend your movement travelling to any other town by the sea.
- At the end of your Movement Phase, gain 1 \gls{fp}.
    * Your maximum \glspl{fp} score is equal to your Wits plus 2.

## Combat

1. Either combatant can turn any number of Item Cards face up to use them.
1. The player lays one of their \glspl{rps} face down.
1. The \gls{nemesis} plays theirs.
    * Both cards are turned up.
1. Observe results:
    * Rock: score is equal to Strength.
    * Paper: score is equal to Wits.
    * Scissors: score is equal to Dexterity.
1. Whoever wins the \gls{rps} game, adds a bonus to their stats equal to the number of combat rounds so far.
    - On the first round, they gain a +1 bonus, on the second, a +2 bonus, and so on...
    - If nobody wins, continue as normal.
1. The winner picks a result:
    * Run away: combat stops.
    * Deal Damage equal to the higher score, minus the lowest.
        - If this is negative, ignore it, and start a new round of combat.
        - \Glspl{character} with no \glspl{hp} are dead.
        - Alternatively, the opponent can leave a \gls{character} with 1 \gls{hp}, and take any items from them they wish.
1. Repeat until someone is defeated or flees.

---

Imagine the following play:

| Your Play         | Opponent's Play | Result                                      |
|:------------------|:----------------|:--------------------------------------------|
| Rock (3)          | Scissors (2)    | Deal 2 Damage                               |
| Scissors (2)      | Scissors (2)    | No result.                                  |
| Paper (1)         | Rock (2)        | Deal 2 Damage                               |
| Rock (3)          | Paper (3)       | You take 4 Damage                           |
| Rock (3)          | Rock (2)        | Deal 1 Damage                               |

---

Here, you have a Strength Attribute of 3.
You play 'rock', which represents using that Strength Attribute, so your score is 3.
Your opponent plays scissors, which represents their Dexterity Attribute of 2.
That means you win the round, so you gain a +1 Bonus, putting you up to 4.

With your score of 4 against their score of 2, you can deal 2 Damage to them.

### \Glsentrytext{monster} Treasures

When you kill a \gls{monster}, pick up a number of Item Cards equal to its Treasure Rating.

## Items in Combat

- Item Cards can boost your Attributes while in Combat.
    * If multiple Item Cards boost an Attribute, you can only use 1 at a time.

# Stats

## \Glsfmtlongpl{fp}

- Your maximum \glspl{fp}
- Any time you would receive Damage, you can spend a FP to ignore all of it.

## \Glsfmtlongpl{hp}

- Your maximum \glspl{hp} score is equal to your Strength + 2.
- \Glspl{character} without \glspl{hp} die.
    * The player then draws a new \gls{character} Card and Quest Card, then places their \gls{pc} on the \gls{tavern}.

# Conditions

### \Glsfmtlong{pc} Death

- If \pgls{npc} dies, the current \gls{nemesis} draws a new \gls{character} Card to replace them, and puts that Card on the board, using the same restrictions as during Setup.
- The board will always contain a green \gls{npc}, but anyone who had a mission to kill 'the Green \gls{npc}' still counts as having completed their mission.
- All other people with Quest cards which mention the Green \gls{npc} must now go after this new \gls{npc}.
- The \gls{npc}'s items can all be taken by the killer, but disappear as soon as their hex becomes empty.

### Items

- Just as above, when a Quest Item leaves the table, the current \gls{nemesis} replaces it with another.

# Magic {#magic}

- You can target anyone on an adjacent hex.
- To cast a spell, spend a number of \glspl{fp}
- \Glspl{monster} can only cast spells by spending the \gls{nemesis}' \glspl{fp}

## Aldaron

### Aldaron 1

- Mist: Target hex is filled with Mist until your next turn. Anyone coming in contact with the hex reduces their Movement score to half (rounded up).

### Aldaron 2

- Nature's Wrath: Target any \gls{monster} with a Wits score lower than yours.  It moves anywhere you choose.

## Conjuration

### Conjuration 1

- Conjure Item: Place any number of Item Cards from your hand into play.

### Conjuration 2

- Teleport: Move 2 hexes in any direction.  Ignore the terrain type.

## Enchantment Sphere

### Enchantment 1

- Aura: Gain +2 Wits until the end of your next turn.
- Guile: If you would be able to barter normally, then you can barter immediately. You can subtract your Persuasion score from the value of any item on the Barter table to make it cheaper.

### Enchantment 2

- Bamboozle: Target takes a -2 Wits penalty until they decide to Explore.

## Invocation

## Fate

### Fate 1

- Glimpse: Look at any face down card.
- Spy: Look at any player's hand.

### Fate 2

- Oracle: Pick up the top 3 cards of any deck, look at them, and put them back in any order.

## Invocation

### Invocation 1

- Flamebolt: Deal 2 Damage to any target.

### Invocation 2

- Fireball: Deal 3 Damage to any target.

## Necromancy

### Necromancy 1

- Summon Ghoul: Place a ghoul token on target hex.

### Necromancy 2

Remove all FP from a target.

